export const state = () => ({
  isGuestLogin: false,
  isLogin: false,
  forceLogin: false,
  user: {
    id: '',
    email: '',
    tel: '',
    address: {
      address: '',
      city: '',
      state: '',
      zipCode: ''
    },
    lastName: '',
    loginType: '',
    restaurantId: '',
    status: '',
    uidSocial: ''
  },
  accessToken: ''
})

export const actions = {
  logInWithGuest ({ commit }) {
    commit('LOGIN_GUEST')
  },
  logIn ({ commit }) {
    commit('LOGIN')
  },
  logOut ({ commit }) {
    commit('LOGOUT')
  },
  setCustomer ({ commit }, payload) {
    commit('SET_CUSTOMER', payload)
  },
  setAccessToken ({ commit }, payload) {
    commit('SET_TOKEN', payload)
  },
  setRememberUser ({ commit }, payload) {
    commit('SET_RMB', payload)
  },
  setForceLogin ({ commit }) {
    commit('SET_FLOGIN')
  },
  reset ({ commit }) {
    commit('RESET')
  }
}

export const mutations = {
  SET_CUSTOMER (state, payload) {
    state.user = payload
  },
  LOGIN_GUEST (state) {
    state.isGuestLogin = true
    state.isLogin = true
    state.forceLogin = false
  },
  LOGIN (state) {
    state.isLogin = true
    state.forceLogin = false
  },
  LOGOUT (state) {
    state.isGuestLogin = false
    state.isLogin = false
    state.forceLogin = false
  },
  SET_TOKEN (state, payload) {
    state.accessToken = payload
  },
  SET_RMB (state, payload) {
    state.remember = payload
  },
  SET_FLOGIN (state) {
    state.forceLogin = true
  },
  RESET (state) {
    state.user = {
      id: '',
      email: '',
      tel: '',
      address: {
        address: '',
        city: '',
        state: '',
        zipCode: ''
      },
      lastName: '',
      loginType: '',
      restaurantId: '',
      status: '',
      uidSocial: ''
    }
    state.accessToken = ''
    state.isGuestLogin = false
    state.isLogin = false
    state.forceLogin = false
  }
}

export const getters = {
  isLogin: (state) => state.isLogin,
  isGuestLogin: (state) => state.isGuestLogin,
  user: (state) => state.user,
  firstName: (state) => state.user.firstName,
  lastName: (state) => state.user.lastName,
  fullName: (state) => `${state.user.firstName } ${ state.user.lastName}`,
  email: (state) => state.user.email,
  tel: (state) => state.user.tel,
  addresses: (state) => state.user.address,
  address: (state) => state.user.address.address,
  city: (state) => state.user.address.city,
  state: (state) => state.user.address.state,
  zipCode: (state) => state.user.address.zipCode,
  id: (state) => state.user.id,
  token: (state) => state.accessToken,
  forceLogin: (state) => state.forceLogin,
  loginType: (state) => state.user.loginType
}
