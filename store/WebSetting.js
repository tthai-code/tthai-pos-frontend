import Css2Json from 'json-to-css'

export const state = () => ({
  facebookUrl: null,
  whatAppsUrl: null,
  logoUrl: null,
  favIconUrl: null,
  subDomain: '',
  bannerImageUrl: null,
  contrastColor: '',
  hoverColor: '',
  mainColor: '',
  domain: '',
  isReservation: false,
  isOnlineOrdering: false,
  hours: [
    {
      label: 'MONDAY',
      isClosed: true,
      period: []
    },
    {
      label: 'TUESDAY',
      isClosed: true,
      period: []
    },
    {
      label: 'WEDNESDAY',
      isClosed: true,
      period: []
    },
    {
      label: 'THURSDAY',
      isClosed: true,
      period: []
    },
    {
      label: 'FRIDAY',
      isClosed: true,
      period: []
    },
    {
      label: 'SATURDAY',
      isClosed: true,
      period: []
    },
    {
      label: 'SUNDAY',
      isClosed: true,
      period: []
    }
  ],
  restaurantId: '',
  restaurant: {
    defaultServiceCharge: 0,
    taxRate: {
      isAlcoholTaxActive: false,
      alcoholTax: 0,
      tax: 0
    },
    businessPhone: '',
    address: {
      zipCode: '',
      state: '',
      city: '',
      address: ''
    },
    dba: '',
    legalBusinessName: '',
    id: ''
  },
  css: ''
})

export const actions = {
  setSetting ({ commit, dispatch }, payLoad) {
    commit('SET_WEB_SETTING', payLoad)
    dispatch('setCss', {
      contrastColor: payLoad.contrastColor,
      hoverColor: payLoad.hoverColor,
      mainColor: payLoad.mainColor
    })
  },
  setCss ({ commit }, payload) {
    const cssJson = {
      ':root': {
        '--color-primary': payload.mainColor ? `${payload.mainColor} !important` : '',
        '--color-hover': payload.hoverColor ? `${payload.hoverColor} !important` : '',
        '--color-contrast': payload.contrastColor ? `${payload.contrastColor} !important` : ''
      }
    }
    commit('SET_CSS', Css2Json.of(cssJson))
  }
}

export const mutations = {
  SET_WEB_SETTING (state, payload) {
    state.facebookUrl = payload.facebookUrl
    state.whatAppsUrl = payload.whatAppsUrl
    state.logoUrl = payload.logoUrl
    state.favIconUrl = payload.favIconUrl
    state.subDomain = payload.subDomain
    state.bannerImageUrl = payload.bannerImageUrl
    state.contrastColor = payload.contrastColor
    state.hoverColor = payload.hoverColor
    state.mainColor = payload.mainColor
    state.domain = payload.domain
    state.isReservation = payload.isReservation
    state.isOnlineOrdering = payload.isOnlineOrdering
    state.restaurantId = payload.restaurantId

    state.hours = payload.hours

    state.restaurant.defaultServiceCharge = payload.restaurant.defaultServiceCharge
    state.restaurant.taxRate.isAlcoholTaxActive = payload.restaurant.taxRate.isAlcoholTaxActive
    state.restaurant.taxRate.alcoholTax = payload.restaurant.taxRate.alcoholTax
    state.restaurant.taxRate.tax = payload.restaurant.taxRate.tax
    state.restaurant.businessPhone = payload.restaurant.businessPhone
    state.restaurant.address.zipCode = payload.restaurant.address.zipCode
    state.restaurant.address.state = payload.restaurant.address.state
    state.restaurant.address.city = payload.restaurant.address.city
    state.restaurant.address.address = payload.restaurant.address.address
    state.restaurant.dba = payload.restaurant.dba
    state.restaurant.legalBusinessName = payload.restaurant.legalBusinessName
    state.restaurant.id = payload.restaurant.id
  },
  SET_CSS (state, payload) {
    state.css = payload
  }
}

export const getters = {
  facebookUrl: (state) => state.facebookUrl,
  whatAppsUrl: (state) => state.whatAppsUrl,
  logoUrl: (state) => state.logoUrl,
  favIconUrl: (state) => state.favIconUrl || state.logoUrl || '/svg/appBarLogo.svg',
  subDomain: (state) => state.subDomain,
  bannerImageUrl: (state) => state.bannerImageUrl,
  contrastColor: (state) => state.contrastColor,
  hoverColor: (state) => state.hoverColor,
  mainColor: (state) => state.mainColor,
  domain: (state) => state.domain,
  isReservation: (state) => state.isReservation,
  isOnlineOrdering: (state) => state.isOnlineOrdering,
  restaurantId: (state) => state.restaurantId,
  restaurant: (state) => state.restaurant,
  taxRate: (state) => state.restaurant.taxRate,
  businessPhone: (state) => state.restaurant.businessPhone,
  addresses: (state) => state.restaurant.address,
  address: (state) => state.restaurant.address.address,
  city: (state) => state.restaurant.address.city,
  state: (state) => state.restaurant.address.state,
  zipCode: (state) => state.restaurant.address.zipCode,
  dba: (state) => state.restaurant.dba,
  legalBusinessName: (state) => state.restaurant.legalBusinessName,
  serviceCharge: (state) => state.restaurant.defaultServiceCharge,
  css: (state) => state.css,
  formatPhone: (state) => {
    const empty = ''
    const start = state.restaurant.businessPhone.slice(0, 3)
    const mid = state.restaurant.businessPhone.slice(3, 6)
    const end = state.restaurant.businessPhone.slice(6)
    return empty.concat(empty, '(', start, ') ', mid, '-', end)
  },
  formatFacebookUrl: (state) => state.facebookUrl?.replace(/(?:(?:http|https):\/\/)?(?:www.)?facebook.com\//, ''),
  hours: (state) => state.hours
}
