import dayjs from '@/plugins/Dayjs'

const formatter = new Intl.NumberFormat('en-US', {
  style: 'currency',
  currency: 'USD'
})

export const state = () => ({
  items: [],
  expire: null,
  tax: 0,
  alcoholTax: 0,
  serviceCharge: 0,
  convenienceFee: 0,
  deliveryFee: 0,
  tips: 0,
  orderMethod: 'PICKUP',
  date: '',
  time: '00:00 - 00:00',
  state: '',
  orderId: '',
  data: {},
  action: null,
  tempNetTotal: 0
})

export const actions = {
  addToCart ({ state, commit, dispatch }, payload) {
    const itemList = JSON.parse(JSON.stringify(state.items))
    const selected = JSON.parse(JSON.stringify(payload.selected))
    selected.activeMod = payload.newMod
    selected.note = payload.newNote
    const index = itemList.findIndex((item) => {
      const oldItemMod = JSON.stringify(item.activeMod)
      const selectedMod = JSON.stringify(selected.activeMod)
      return item.id === payload.selected.id
      && oldItemMod === selectedMod
      && item.note === selected.note
    })
    if (index === -1) {
      selected.amount = 1
      commit('SET_CART_ITEMS', [
        ...itemList,
        { ...selected, total: selected.price, amount: payload.amount }
      ])
    } else {
      dispatch('setAmount', {
        index,
        amount: itemList[index].amount + payload.amount
      })
    }
  },
  removeFromCart ({ dispatch, commit }, index) {
    dispatch('setAmount', { index, amount: 0 })
    commit('REMOVE', index)
  },
  setMod ({ commit }, payload) {
    commit('SET_MOD', {
      index: payload.index,
      mod: payload.mod
    })
  },
  setNote ({ commit }, payload) {
    commit('SET_NOTE', {
      index: payload.index,
      note: payload.note
    })
  },
  setAmount ({ commit, dispatch }, payload) {
    commit('SET_AMOUNT', {
      index: payload.index,
      value: payload.amount
    })
    dispatch('setItemTotal', payload.index)
  },
  setItemTotal ({ state, commit }, index) {
    const value = state.items[index].price * state.items[index].amount
    commit('SET_ITEM_TOTAL', { index, value })
  },
  setTips ({ commit }, val) {
    commit('SET_TIPS', val)
  },
  setOrderMethod ({ commit }, val) {
    commit('SET_ORDER_METHOD', val)
    if (val === 'PICKUP') {
      commit('SET_DELIVERY_FEE', 0)
    } else {
      commit('SET_DELIVERY_FEE', 5)
    }
  },
  setDateTime ({ commit }, payload) {
    commit('SET_TIME', {
      date: payload.date,
      time: payload.time
    })
  },
  setState ({ commit }, val) {
    commit('SET_STATE', val)
  },
  setTax ({ commit }, payload) {
    commit('SET_TAX', payload.tax)
    if (payload.isAlcoholTaxActive) {
      commit('SET_ALCOHOL_TAX', payload.alcoholTax)
    }
  },
  setServiceCharge ({ commit }, payload) {
    commit('SET_SERVICE_CHARGE', payload)
  },
  setOrderId ({ commit }, payload) {
    commit('SET_ORDER_ID', payload)
  },
  setData ({ commit }, payload) {
    commit('SET_DATA', payload)
  },
  resetCart ({ commit }) {
    commit('RESET')
  },
  setAction ({ commit }, payload) {
    commit('SET_ACTION', payload)
  },
  setExpire ({ commit }, payload) {
    commit('SET_EXPIRE', payload)
  },
  setTempNetTotal ({ commit }, payload) {
    commit('SET_TEMP_NET', payload)
  },
  setConveninenceFee ({ commit }, payload) {
    commit('SET_CONVENINENCE_FEE', payload)
  }
}

export const mutations = {
  RESET (state) {
    state.items = []
    state.expire = null
  },
  SET_CART_ITEMS (state, payload) {
    state.items = payload
  },
  ADD_NEW (state, payload) {
    state.items.push(payload)
  },
  ADD (state, payload) {
    state.items[payload].amount++
  },
  REMOVE (state, payload) {
    state.items.splice(payload, 1)
  },
  SET_MOD (state, payload) {
    state.items[payload.index].activeMod = payload.mod
  },
  SET_NOTE (state, payload) {
    state.items[payload.index].note = payload.note
  },
  SET_AMOUNT (state, payload) {
    state.items[payload.index].amount = payload.value
  },
  SET_ITEM_TOTAL (state, payload) {
    state.items[payload.index].total = payload.value
  },
  SET_TIPS (state, payload) {
    state.tips = payload
  },
  SET_ORDER_METHOD (state, payload) {
    state.orderMethod = payload
  },
  SET_TIME (state, payload) {
    state.date = payload.date
    state.time = payload.time
  },
  SET_STATE (state, payload) {
    state.state = payload
  },
  SET_CONVENINENCE_FEE (state, payload) {
    state.convenienceFee = payload || 0
  },
  SET_TAX (state, payload) {
    state.tax = payload || 0
  },
  SET_DELIVERY_FEE (state, payload) {
    state.deliveryFee = payload || 0
  },
  SET_ALCOHOL_TAX (state, payload) {
    state.alcoholTax = payload || 0
  },
  SET_SERVICE_CHARGE (state, payload) {
    state.serviceCharge = payload || 0
  },
  SET_ORDER_ID (state, payload) {
    state.orderId = payload
  },
  SET_DATA (state, payload) {
    state.data = payload
  },
  SET_ACTION (state, payload) {
    state.action = payload
  },
  SET_EXPIRE (state, payload) {
    state.expire = payload
  },
  SET_TEMP_NET (state, payload) {
    state.tempNetTotal = payload
  }
}

export const getters = {
  sumAmountItem: (state) => {
    const summary = {
      itemInCart: 0,
      subTotal: 0,
      alcoholTotal: 0
    }

    for (let i = 0; i < state.items?.length || 0; i++) {
      const item = state.items[i]
      summary.itemInCart += item.amount || 0

      let modTotal = 0

      if (item.activeMod && item.activeMod.length) {
        for (let j = 0; j < item.activeMod.length || 0; j++) {
          const mod = item.activeMod[j]
          modTotal += mod.price
        }
      }
      summary.subTotal += ((item.price + modTotal) * item.amount)
      if (item.isContainAlcohol) {
        summary.alcoholTotal += (item.price * item.amount)
      }
    }

    return summary
  },
  itemInCart: (state, getter) => getter?.sumAmountItem?.itemInCart || 0,
  subTotal: (state, getter) => getter?.sumAmountItem?.subTotal || 0,
  alcoholTotal: (state, getter) => getter?.sumAmountItem?.alcoholTotal || 0,
  items: (state) => state.items,
  tax: (state, getter) => {
    const { subTotal } = getter
    let taxPct = (state.tax / 100) * subTotal
    taxPct = taxPct.toFixed(2)
    return Number(taxPct) || 0
  },
  alcoholTax: () => 0,
  serviceCharge: (state) => state.serviceCharge,
  serviceChargeAmount: (state, getter) => {
    const { netTotal, serviceCharge } = getter
    if (netTotal === 0) {
      return 0
    }

    const newNet = Number(netTotal.replace('$', ''))
    return (newNet * (serviceCharge / 100))
  },
  convenienceFee: (state) => {
    // if (!state.convenienceFee) return 0
    // const { subTotal, tax, serviceCharge } = getter
    // let cfPct = (state.convenienceFee / 100) * (subTotal + tax + serviceCharge)
    // cfPct = cfPct.toFixed(2)
    // return Number(cfPct) || 0
    if (state.convenienceFee) {
      return state.convenienceFee
    }
    return 0
  },
  deliveryFee: (state) => state.deliveryFee,
  netTotal: (state, getter) => {
    let netTotal = 0
    const { itemInCart, receipt, tempNetTotal } = getter
    if (itemInCart === 0) {
      if (tempNetTotal && state.state === 'payment') {
        return formatter.format(tempNetTotal)
      }
      return formatter.format(0)
    }
    receipt.map((item) => item.numValue).forEach((val) => { netTotal += val })
    return formatter.format(netTotal)
  },
  tips: (state) => Number(Number(state.tips).toFixed(2)),
  postTips: (state, getter) => {
    const { tips, netTotal, convenienceFee, serviceCharge } = getter
    if (netTotal === 0) {
      return 0
    }
    if (tips === 0) {
      return netTotal
    }

    const newNet = Number(netTotal.replace('$', ''))

    const total = newNet + tips + convenienceFee + serviceCharge

    return formatter.format(total)
  },
  pickupDateTime: (state) => {
    const time = state.time.split(' ')
    const nDate = `${state.date} ${time[0]}`
    const dateTime = dayjs(nDate, 'YYYY-MM-DD hh:mmA').toISOString()
    return dateTime
  },
  orderMethod: (state) => state.orderMethod,
  date: (state) => state.date,
  time: (state) => state.time,
  state: (state) => state.state,
  data: (state) => state.data,
  orderId: (state) => state.orderId,
  receipt: (state, getter) => {
    const { subTotal, convenienceFee, tax, deliveryFee } = getter
    const bill = [{
      title: 'Subtotal',
      value: formatter.format(subTotal),
      numValue: subTotal
    },
    {
      title: 'Tax',
      value: formatter.format(tax),
      numValue: tax
    },
    // {
    //   title: 'Alcohol Tax',
    //   value: formatter.format(alcoholTax)
    //   numValue: alcoholTax
    // },
    // {
    //   title: 'Service Charge',
    //   value: formatter.format(serviceCharge)
    //   numValue: serviceCharge
    // },
    {
      title: 'Convenience Fee',
      value: formatter.format(convenienceFee),
      numValue: convenienceFee
    },
    {
      title: 'Delivery Fee',
      value: formatter.format(deliveryFee),
      numValue: deliveryFee
    }]
    return bill
  },
  action: (state) => state.action,
  expire: (state) => state.expire,
  tempNetTotal: (state) => state.tempNetTotal
}
