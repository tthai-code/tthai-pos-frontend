import WebSettingProvider from '@/resources/WebSettingProvider'

const WebSettingService = new WebSettingProvider()

export const state = () => ({
  domain: null,
  current: ''
})

export const actions = {
  async nuxtServerInit ({ dispatch, commit }, { req }) {
    const domain = req.headers.host
    const data = await WebSettingService.getSetting({ domain })
    await dispatch('WebSetting/setSetting', data)
    commit('SET_DOMAIN', domain)
  },
  setCurrent ({ commit }, payload) {
    commit('SET_CURRENT', payload)
  }
}

export const mutations = {
  SET_DOMAIN (state, payload) {
    state.domain = payload
  },
  SET_CURRENT (state, payload) {
    state.current = payload
  }
}

export const getters = {
  domain: (state) => (state.domain),
  current: (state) => (state.current)
}
