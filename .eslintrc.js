module.exports = {
  root: true,
  env: {
    browser: true,
    es6: true,
    node: true
  },
  extends: [
    'plugin:vue/recommended',
    'airbnb-base'
  ],
  rules: {
    'vue/max-len': ['error', {
      code: 150,
      tabWidth: 2,
      ignoreUrls: true,
      ignoreRegExpLiterals: true,
      ignoreComments: true,
      ignoreTrailingComments: true,
      ignoreStrings: true,
      ignoreHTMLTextContents: true,
      ignoreHTMLAttributeValues: true
    }],
    'max-len': 'off',
    'radix': ['error', 'as-needed'],
    'template-curly-spacing': 'off',
    'indent': ['error', 2, { SwitchCase: 1 }],
    'no-plusplus': 'off',
    'no-console': process.env.NODE_ENV === 'production' ? ['error', { allow: ['warn', 'error'] }] : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? ['error'] : 'off',
    'vue/html-closing-bracket-newline': 'off',
    'vue/html-self-closing': 'off',
    'vue/component-name-in-template-casing': 'off',
    'comma-dangle': ['error', 'never'],
    'semi': ['error', 'never'],
    'quote-props': ['error', 'consistent-as-needed'],
    'import/no-unresolved': 'off',
    'space-before-function-paren': ['error', 'always'],
    'object-curly-newline': 'off',
    'no-underscore-dangle': 'off',
    'no-shadow': ['error', { allow: ['state'] }],
    'prefer-destructuring': ['error', { object: true, array: false }],
    'no-param-reassign': ['error', {
      props: true,
      ignorePropertyModificationsFor: [
        'acc', // for reduce accumulators
        'state',
        'el',
        'Vue',
        'event'
      ]
    }],
    'no-restricted-syntax': ['error', 'ForInStatement', 'LabeledStatement', 'WithStatement'],
    'no-bitwise': ['error', { int32Hint: true }],
    'import/extensions': [0, { '<js>': 'always' }]
  },
  parserOptions: {
    parser: '@babel/eslint-parser',
    allowImportExportEverywhere: true
  },
  settings: {
    'import/core-modules': ['vue', 'vuex'], // these modules are included in nuxt.js
    'requireConfigFile': false
    // 'import/resolver': {
    //   jsconfig: { config: 'jsconfig.json' },
    //   node: { extensions: ['.js', '.jsx'] }
    // }
  },
  globals: {
    $nuxt: 'readonly'
  }
}
