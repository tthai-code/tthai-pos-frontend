export const getStore = () => (process.browser ? window.$nuxt.$store : null)

export const getMemberToken = () => {
  const store = getStore()
  if (store) {
    const accessToken = store.getters['User/token']
    return {
      key: 'Authorization',
      value: `Bearer ${accessToken}`
    }
  }
  return null
}
