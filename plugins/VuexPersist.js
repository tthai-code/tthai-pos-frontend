import VuexPersistence from 'vuex-persist'

export default ({ store }) => {
  new VuexPersistence({
    key: process.env.NODE_ENV === 'production' ? 'TThai.co_1' : 'TThai.co_DEV',
    saveState: (key, state) => {
      const data = JSON.stringify({
        User: state.User,
        Cart: {
          items: state.Cart.items,
          expire: state.Cart.expire
        }
      })
      localStorage.setItem(key, data)
    },
    restoreState: (key, storage) => {
      localStorage.removeItem('TThai')
      if (storage[key]) {
        const data = JSON.parse(storage[key])
        return {
          User: data?.User || {},
          Cart: {
            items: data?.Cart?.items || [],
            expire: data?.Cart?.expire || []
          }
        }
      }
      return {}
    },
    modules: [
      'User',
      'Cart'
    ]
  }).plugin(store)
}
