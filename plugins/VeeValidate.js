import Vue from 'vue'
import { ValidationProvider, ValidationObserver, extend } from 'vee-validate'
import * as rules from 'vee-validate/dist/rules'

Object.keys(rules).forEach((rule) => {
  extend(rule, rules[rule])
})

extend('password', {
  params: ['target'],
  validate (value, { target }) {
    return value === target
  },
  message: 'Password confirmation does not match'
})

extend('tel', {
  validate (value) {
    if (!value) return false
    return /^\d{3}[-]?\d{3}[-]?\d{4}$/.test(value)
  },
  message: 'Telephone number is invalid'
})

extend('cardMonth', {
  validate (value) {
    if (!value) return false
    if (value.length !== 2) return false
    return (value > 0) && (value <= 12)
  },
  message: 'Expire Month is invalid'
})

extend('cardYear', {
  validate (value) {
    if (!value) return false
    if (value.length !== 2) return false
    return true
  },
  message: 'Expire Year is invalid'
})

extend('cvv', {
  validate (value) {
    if (!value) return false
    return (value.length >= 3) && (value.length <= 4)
  },
  message: 'CVV is invalid'
})

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)
