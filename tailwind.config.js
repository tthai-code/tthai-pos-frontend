/* eslint-disable import/no-extraneous-dependencies */
/** @type {import('tailwindcss').Config} */

const aspectRatio = require('@tailwindcss/aspect-ratio')

module.exports = {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}'
  ],
  theme: {
    fontFamily: {
      sans: ['Arial', 'sans-serif'],
      serif: ['Arial', 'sans-serif'],
      mono: ['Arial', 'sans-serif'],
      body: ['Arial', 'sans-serif'],
      display: ['Arial', 'sans-serif']
    },
    container: {
      center: true,
      padding: '0.75rem'
    },
    screens: {
      'xs': { max: '639px' },
      'sm': { min: '640px' },
      'md': { min: '768px' },
      'lg': { min: '1024px' },
      'xl': { min: '1280px' },
      '2xl': { min: '1536px' }
    },
    extend: {
      colors: {
        primary: 'var(--color-primary)',
        secondary: 'var(--color-secondary)',
        third: 'var(--color-third)',
        info: 'var(--color-info)',
        login: 'var(--color-login)',
        confirm: 'var(--color-confirm)',
        danger: 'var(--color-danger)',
        offline: 'var(--color-offline)',
        hover: 'var(--color-hover)',
        contrast: 'var(--color-contrast)'
      },
      aspectRatio: {
        '4/3': '4 / 3'
      }
    }
  },
  plugins: [
    aspectRatio
  ]
}
