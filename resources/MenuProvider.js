import HttpRequest from './HttpRequest'

class MenuProvider extends HttpRequest {
  getMenuList (query) {
    return this.get('/v1/public/online/menu', query)
  }

  getMenuByID (id, query) {
    return this.get(`/v1/public/online/menu/${id}`, query)
  }

  getCategoryList (query) {
    return this.get('/v1/public/online/menu-category', query)
  }

  getOnlyCategory (query) {
    return this.get('/v1/public/online/menu-category/list', query)
  }

  getCategoryByID (id, query) {
    return this.get(`/v1/public/online/menu-category/${id}/category`, query)
  }
}

export default MenuProvider
