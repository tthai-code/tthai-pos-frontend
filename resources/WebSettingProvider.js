import HttpRequest from './HttpRequest'

const config = {
  auth: {
    username: 'tthai_pos_api',
    password: '0#3hWwqS3iDVI1ou%JHrbFlY0v$'
  }
}

class WebSettingProvider extends HttpRequest {
  getSetting (payLoad) {
    return this.post('/v1/online/web-setting', payLoad, config)
  }
}

export default WebSettingProvider
