import HttpRequest from './HttpRequest'
import { getMemberToken } from '@/utils/GetAuth'

class UserProvider extends HttpRequest {
  normalLogin (query) {
    return this.login('/v1/public/online/customer/login', query)
  }

  guestLogin (query) {
    return this.login('/v1/public/online/customer/guest', query)
  }

  createNewCustomer (query) {
    return this.post('/v1/public/online/customer', query)
  }

  getCustomerById (customerId) {
    return this.get(`/v1/public/online/customer/${customerId}`)
  }

  updateCustomerInfomation (payload) {
    this.setHeader(getMemberToken())
    return this.put('/v1/public/online/customer/info', payload)
  }
}

export default UserProvider
