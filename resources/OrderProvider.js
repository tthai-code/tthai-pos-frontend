import HttpRequest from './HttpRequest'
import { getMemberToken } from '@/utils/GetAuth'

class OrderProvider extends HttpRequest {
  createOrder (payload) {
    this.setHeader(getMemberToken())
    return this.post('/v1/public/online-order/checkout', payload)
  }

  updateOrder (id, body) {
    this.setHeader(getMemberToken())
    return this.patch(`/v1/public/online-order/${id}/paid`, body)
  }

  getOrderAll (query) {
    this.setHeader(getMemberToken())
    return this.get('/v1/public/online-order/history', query)
  }

  getOrderByID (id) {
    this.setHeader(getMemberToken())
    return this.get(`/v1/public/online-order/${id}`)
  }
}

export default OrderProvider
