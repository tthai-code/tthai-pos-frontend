import redirectSSL from 'redirect-ssl'

export default {
  server: {
    host: '0.0.0.0', // default: localhost
    port: process.env.PORT || 3000
  },
  // Target: https://go.nuxtjs.dev/config-target
  target: 'server',

  serverMiddleware: [
    redirectSSL.create({
      enabled: false
    })
  ],

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'TThaiPOS',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'viewport-fit=cover, width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/static/favicon.png' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/main.css',
    '@mdi/font/css/materialdesignicons.min.css',
    'vue-slick-carousel/dist/vue-slick-carousel.css',
    'vue-slick-carousel/dist/vue-slick-carousel-theme.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '@/plugins/Dayjs.js' },
    { src: '@/plugins/VeeValidate.js', mode: 'client' },
    { src: '@/plugins/VuexPersist', mode: 'client' },
    { src: '@/plugins/VueSlickCarousel.js' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: false,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '@nuxtjs/dotenv',
    '@nuxtjs/google-fonts',
    '@nuxt/postcss8'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {}
      }
    },
    transpile: [
      'vee-validate/dist/rules'
    ]
  },

  dotenv: {
    filename: process.env.NODE_ENV === 'production' ? '.env.production' : '.env',
    apiUrl: process.env.NUXT_ENV_API_URL
  },

  googleFonts: {
    families: {
      Poppins: true
    },
    display: 'swap'
  }
}
